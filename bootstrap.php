<?

require_once 'vendor/autoload.php';


use App\AppContainer;
use App\Database\MySQLConnection;

AppContainer::push('config', require_once 'app/config/config.php');

AppContainer::push('connection', new MySQLConnection());