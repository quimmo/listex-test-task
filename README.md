### What is this repository for? ###

Listex test task

### Installation and running ###

* Composer install for enable PSR-4 autoloading
* Put your MySQL config in config/config.php
* Mb you will be needed to disable secure-file-priv in MySQL.
* Just go to the root of the project (simply run the index.php as entrypoint to get the app started and get your table seeded)
