<?

namespace App\Repositories;

use App\Database\Connection;
use App\AppContainer;

class GoodsRepository {

    private $pdo;
    private $table = 'Lst_Goods';

    public function __construct(Connection $connection)
    {
        $this->pdo = $connection->connect(...array_values(AppContainer::get('config')['database']));
    }

    public function importCsv($filePath){
        $statement = $this->pdo->prepare(
            "LOAD DATA LOCAL INFILE ? 
            INTO TABLE {$this->table}
            CHARACTER SET UTF8
            fields terminated BY \",\"
            optionally enclosed by '\"'
            lines terminated BY \"\n\"
            IGNORE 1 ROWS
            (name)
            SET Name = name"
        );
        $statement->execute([$filePath]);
    }

}