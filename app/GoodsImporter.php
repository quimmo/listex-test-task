<?

namespace App;

use App\Repositories\GoodsRepository;

class GoodsImporter {


    public function import(){
        $goodsRepository = new GoodsRepository(AppContainer::get('connection'));
        $initialCsv = fopen('static/price.csv', 'r');
        $resultCsv = fopen('static/final.csv', 'w+');
        $line = fgetcsv($initialCsv, 0, ',');
        $line[1] = "Name";
        fputcsv($resultCsv, array_slice($line, 1, 1), ',', '"');
        while (!feof($initialCsv)) {
            $line = fgetcsv($initialCsv, 0, ',');
            fputcsv($resultCsv, array_slice($line, 1, 1), ',', '"');
        }
        $goodsRepository->importCsv($_SERVER['DOCUMENT_ROOT'].'/static/final.csv');
    }

}