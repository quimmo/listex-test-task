<?

namespace App\Database;

interface Connection {
    public function connect($host, $user, $password, $database);
}

