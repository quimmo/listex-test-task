<?

namespace App\Database;

use App\AppContainer;

class MySQLConnection implements Connection {

    public function connect($host, $user, $password, $database)
    {
        try {
            return new \PDO("mysql:host={$host};dbname={$database}", $user, $password, [
                \PDO::MYSQL_ATTR_LOCAL_INFILE => 1,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            ]);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}